//
//  SinOscillator.cpp
//  JuceBasicAudio
//
//  Created by Tom Sanger on 13/11/2014.
//
//

#include "SinOscillator.h"

SinOscillator::SinOscillator()
{
    frequency = 40.f;
    phasePosition = 0.f;
}
SinOscillator::~SinOscillator()
{
    
}

float SinOscillator::getSample()
{
    phasePosition += phaseIncrement;
    
    if (phasePosition > twoPi)
        phasePosition -= twoPi;
    
    return phasePosition;
}

void SinOscillator::setFrequency(float frequencyIn)
{
    frequency = frequencyIn;
    phaseIncrement = (twoPi * frequency)/currentSampleRate;
}

void SinOscillator::setAmplitude(float gainIn)
{
    
}

void SinOscillator::getSampleRate(float sampleRateIn)
{
    currentSampleRate = sampleRateIn;
    phaseIncrement = (twoPi * frequency)/currentSampleRate;
}
