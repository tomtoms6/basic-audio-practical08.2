//
//  SinOscillator.h
//  JuceBasicAudio
//
//  Created by Tom Sanger on 13/11/2014.
//
//

#ifndef __JuceBasicAudio__SinOscillator__
#define __JuceBasicAudio__SinOscillator__

#include <stdio.h>
#include "../../JuceLibraryCode/JuceHeader.h"

class SinOscillator

{
public:
    SinOscillator();
    ~SinOscillator();
    
    float getSample();
    void setFrequency(float frequencyIn);
    void setAmplitude(float gain);
    void getSampleRate(float sampleRateIn);
    
private:
    float twoPi = 2 * M_PI;
    float frequency, phasePosition, currentSampleRate;
    float phaseIncrement; // = (twoPi * frequency)/currentSampleRate;
};


#endif /* defined(__JuceBasicAudio__SinOscillator__) */

