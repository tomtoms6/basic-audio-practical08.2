/*
  ==============================================================================

    Audio.cpp
    Created: 13 Nov 2014 8:14:40am
    Author:  Tom Mitchell

  ==============================================================================
*/

#include "Audio.h"

Audio::Audio()
{
    audioDeviceManager.initialiseWithDefaultDevices (2, 2); //2 inputs, 2 outputs
    
    
    audioDeviceManager.addMidiInputCallback (String::empty, this);
    audioDeviceManager.addAudioCallback (this);
}

Audio::~Audio()
{
    audioDeviceManager.removeAudioCallback (this);
    audioDeviceManager.removeMidiInputCallback (String::empty, this);
}


void Audio::handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message)
{
    //All MIDI inputs arrive here
}

void Audio::audioDeviceIOCallback (const float** inputChannelData,
                                           int numInputChannels,
                                           float** outputChannelData,
                                           int numOutputChannels,
                                           int numSamples)
{
    //All audio processing is done here
    const float *inL = inputChannelData[0];
    const float *inR = inputChannelData[1];
    float *outL = outputChannelData[0];
    float *outR = outputChannelData[1];
    float twoPi = 2 * M_PI;
    //float phaseIncrement = (twoPi * frequency)/sampleRate;
    
    while(numSamples--)
    {
        sharedMemory.enter();
//        
//        phasePosition += phaseIncrement;
//        
//        if (phasePosition > twoPi)
//            phasePosition -= twoPi;
        
        phasePosition = sinOsc.getSample();
        
        *outL = sin(phasePosition) * gain;
        *outR = sin(phasePosition) * gain;
        
        sharedMemory.exit();
        
//        *outL = *inL * gain;
//        *outR = *inR * gain;
        
        inL++;
        inR++;
        outL++;
        outR++;
    }
}


void Audio::audioDeviceAboutToStart (AudioIODevice* device)
{
    sampleRate = device->getCurrentSampleRate();
    sinOsc.getSampleRate(sampleRate);
}

void Audio::audioDeviceStopped()
{

}

void Audio::setGain(float inGain)
{
    gain = inGain;
}

void Audio::setFrequency(float frequencyIn)
{
    sinOsc.setFrequency(frequencyIn);
}

