/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent (Audio& audio_) :  Thread ("CounterThread"),
                                                audio (audio_)
{
    setSize (500, 400);
    
    gainSlider.setSliderStyle(juce::Slider::LinearVertical);
    gainSlider.setBounds(0, 0, getWidth()/7, getHeight()/3);
    gainSlider.setRange(0, 1);
    gainSlider.addListener(this);
    addAndMakeVisible(gainSlider);
    
    freqSlider.setSliderStyle(juce::Slider::LinearVertical);
    freqSlider.setBounds(100, 0, getWidth()/7, getHeight()/3);
    freqSlider.setRange(40, 1000);
    freqSlider.addListener(this);
    addAndMakeVisible(freqSlider);
    
    CounterButton.setBounds(60, 150, getWidth()/5, getHeight()/10);
    CounterButton.addListener(this);
    addAndMakeVisible(CounterButton);
    
    CounterButtonLabel.setText(onOff, dontSendNotification);
    CounterButtonLabel.setBounds(60, 180, getWidth()/5, getHeight()/10);
    addAndMakeVisible(CounterButtonLabel);
    
    TimerLabel.setBounds(60, 200, getWidth()/5, getHeight()/10);
    addAndMakeVisible(TimerLabel);
    
    startThread();
}

MainComponent::~MainComponent()
{
    stopThread(500);
}

void MainComponent::resized()
{
    
}

void MainComponent::sliderValueChanged(juce::Slider *slider)
{
    DBG(gainSlider.getValue());
    
    audio.setGain(gainSlider.getValue());

    audio.setFrequency(freqSlider.getValue());
}

void MainComponent::run()
{
    
    while (!threadShouldExit())
    {
        UInt32 time = Time::getMillisecondCounter();
        std::cout << "Counter:" << Time::getApproximateMillisecondCounter() << "\n";
        Time::waitForMillisecondCounter(time + 100);
       
    }
}

void MainComponent::buttonClicked (Button* button)
{
   
    if(button == &CounterButton)
    {
        if(isThreadRunning())
        {
            
            onOff = "off";
            stopThread(500);
            
        }
        else
        {
            onOff = "on";
            
            startThread();
        }
        
        CounterButtonLabel.setText(onOff, dontSendNotification);
    }


}

void MainComponent::labelTextChanged (Label* labelThatHasChanged)
{
    
}

//MenuBarCallbacks==============================================================
StringArray MainComponent::getMenuBarNames()
{
    const char* const names[] = { "File", 0 };
    return StringArray (names);
}

PopupMenu MainComponent::getMenuForIndex (int topLevelMenuIndex, const String& menuName)
{
    PopupMenu menu;
    if (topLevelMenuIndex == 0)
        menu.addItem(AudioPrefs, "Audio Prefrences", true, false);
    return menu;
}

void MainComponent::menuItemSelected (int menuItemID, int topLevelMenuIndex)
{
    if (topLevelMenuIndex == FileMenu)
    {
        if (menuItemID == AudioPrefs)
        {
            AudioDeviceSelectorComponent audioSettingsComp (audio.getAudioDeviceManager(),
                                                            0, 2, 2, 2, true, true, true, false);
            audioSettingsComp.setSize (450, 350);
            DialogWindow::showModalDialog ("Audio Settings",
                                           &audioSettingsComp, this, Colours::lightgrey, true);
        }
    }
}

